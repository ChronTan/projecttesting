package dm.controller;

import java.util.ArrayList;
import java.util.List;

import dm.model.Employee;
import dm.repo.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping({"/employees"})
@Transactional
public class TestController {

    @Autowired
    EmployeeRepo employeeRepo;

//    private List<Employee> employees = createList();
    private List<Employee> employees = new ArrayList<>();

//    @RequestMapping(value = "/employees", method = RequestMethod.GET, produces = "application/json")
//    public List<Employee> firstPage()
//    {
//        return employees;
//    }

    @GetMapping(produces = "application/json")
    public List<Employee> firstPage() {
//        List<Employee> employees = new ArrayList<>();
//        employeeRepo.save(new Employee("qwe(no delete)", "qwe"));
//        employeeRepo.save(new Employee("zxc", "zxc"));
//        employeeRepo.findAll().forEach(employees::add);
        return employees;
    }


    @DeleteMapping(path = { "/{name}" })
    public Employee delete(@PathVariable("name") String name) {
        Employee deletedEmp = null;
        employeeRepo.findAll().forEach(employees::add);
        for (Employee emp : employees) {
            if (emp.getName().equals(name)) {
                employees.remove(emp);
                employeeRepo.deleteByName(emp.getName());
                deletedEmp = emp;
                break;
            }
        }
        return deletedEmp;
    }

    @PostMapping
    public Employee create(@RequestBody Employee user) {
        employees.add(user);
        employeeRepo.save(user);
        return user;
    }

//    @PutMapping(path = { "/{name}" })
//    public Employee update(@PathVariable("name") String name) {
//        Employee updateEmp = null;
//        employeeRepo.findAll().forEach(employees::add);
//        for (Employee emp : employees) {
//            if (emp.getName().equals(name)) {
//                emp.setEmpId("test");
//                emp.setName("test");
//                emp.setDesignation("test");
//                employeeRepo.save(emp);
//                updateEmp = emp;
//                break;
//            }
//        }
//        return updateEmp;
//    }



    private static List<Employee> createList() {
        List<Employee> tempEmployees = new ArrayList<>();
        Employee emp1 = new Employee();
        emp1.setName("emp1");
        emp1.setDesignation("manager");
        emp1.setEmpId("1");
        emp1.setSalary(3000);

        Employee emp2 = new Employee();
        emp2.setName("emp2");
        emp2.setDesignation("developer");
        emp2.setEmpId("2");
        emp2.setSalary(3000);
        tempEmployees.add(emp1);
        tempEmployees.add(emp2);
        return tempEmployees;
    }

}
