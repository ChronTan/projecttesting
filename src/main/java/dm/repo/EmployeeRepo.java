package dm.repo;

import dm.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepo extends CrudRepository<Employee, Long> {
    List<Employee> findByName(String name);

    List<Employee> deleteByName(String name);

}
